
/*
* ana oyun sahnesini olustur
 */

const messageMap = new Map();
const armyManager = new ArmyManager();

function startGameScreen() {

    isDragging = false;
    document.getElementById("select-country").remove();

    // Yeni bir <div> öğesi oluşturun
    let div = document.createElement("span");
    div.id = "money-box";
    div.className = "money-box";
    div.innerText = money + " 💰";

    document.getElementById("side-area").appendChild(div);


    const ball = document.getElementById('ball');
    const slider = document.getElementById('slider');
    const valueDisplay = document.getElementById('valueDisplay');
    const sliderWidth = slider.offsetWidth - ball.offsetWidth; // Boncuğun kayabileceği maksimum genişlik


    // Fare basılınca boncuğu sürüklemeye başla
    ball.addEventListener('mousedown', function(e) {
        isDragging = true;
    });

    // Fare hareket ettiğinde boncuğu sürükle
    document.addEventListener('mousemove', function(e) {
        if (!isDragging) return;

        let newPosition = e.clientX - slider.getBoundingClientRect().left - (ball.offsetWidth / 2);
        if (newPosition < 0) newPosition = 0; // Sol sınır
        if (newPosition > sliderWidth) newPosition = sliderWidth; // Sağ sınır

        ball.style.left = newPosition + 'px';

        // Seçilen değeri hesapla ve ekranda göster
        valueDisplay.innerText  = Math.round((newPosition / sliderWidth) * 10) +  " 💰";

    });

    // Fare bırakılınca durumu kontrol et
    document.addEventListener('mouseup', function() {

        if (isDragging) {
            isDragging = false;
        }
    });



    // Slider alanına tıklanma olayı ekle
    slider.addEventListener('click', function(e) {
        // Boncuğun konumunu hesapla
        let newPosition = e.clientX - slider.getBoundingClientRect().left - (ball.offsetWidth / 2);
        if (newPosition < 0) newPosition = 0; // Sol sınır
        if (newPosition > sliderWidth) newPosition = sliderWidth; // Sağ sınır

        // Boncuğu yeni konuma yerleştir
        ball.style.left = newPosition + 'px';

        // Seçilen değeri hesapla ve ekranda göster
        valueDisplay.innerText = Math.round((newPosition / sliderWidth) * 10) + "💰";


    });

    // Boncuğu başlangıçta sıfır konumuna ayarla
    ball.style.left = '0px';


}










function addMessageBox(messageId) {
    let spanElement = document.createElement("span");
    spanElement.id = messageId.toString();
    spanElement.innerText = "✉️";
    spanElement.className = "message-icon";


    spanElement.onclick = function () {
        showMessage(messageId);
        messageMap.delete(messageId);
        document.getElementById(messageId).remove()
    }

    document.getElementById("side-area-right").appendChild(spanElement);
}


function showMessage(messageId) {
    const message = messageMap.get(messageId);
    if (message.type === "peaceRequest") {
        let countryId = message.data;

        console.log(countryMap[countryId].name + " sizinle barış yapmak istiyor.");

        let onay = confirm(countryMap[countryId].name + " sizinle barış yapmak istiyor.");
        if (onay) {
            send("acceptPeaceRequest", countryId);
            console.log("barış yapıldı");
        } else {
            send("rejectPeaceRequest", countryId);
            console.log("barış teklifi reddedildi");
        }
    }
    else if (message.type === "allianceRequest") {
        let countryId = message.data;

        console.log(countryMap[countryId].name + " sizinle ittifak olmak istiyor.");

        let onay = confirm(countryMap[countryId].name + " sizinle ittifak olmak istiyor.");
        if (onay) {
            send("acceptAllianceRequest", countryId);
            console.log("ittifak kabul edildi.");
        } else {
            send("rejectAllianceRequest", countryId);
            console.log("ittfak teklifi reddedildi");
        }
    }



    else if (message.type === "standartMessage") {
        let text = message.data;
        alert(text);
    }

}











