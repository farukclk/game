/*
*  zoom settings and other things
*
*/

const svg = document.getElementById('map');
let isDragging = false;
let ekranSuruklendi = false;
let startX, startY, initialTranslateX, initialTranslateY;

let scale = 2.7963396836584136;
let scaleMax = 13;
let scaleMin = 0.9;
const scaleFactor = 0.1;

// Fare ile sürükleme işlemi için olay dinleyicileri
svg.addEventListener('mousedown', (e) => {
    isDragging = true;
    startX = e.clientX;
    startY = e.clientY;
    const transform = svg.getAttribute('transform');
    if (transform) {
        const translate = transform.match(/translate\(([^)]+)\)/);
        if (translate) {
            [initialTranslateX, initialTranslateY] = translate[1].split(',').map(Number);
        }
    } else {
        initialTranslateX = 572;
        initialTranslateY = -497;
    }
});





svg.addEventListener('mousemove', (e) => {

    if (isDragging) {
        ekranSuruklendi = true;
        const dx = e.clientX - startX;
        const dy = e.clientY - startY;
        const translateX = initialTranslateX + dx;
        const translateY = initialTranslateY + dy;
        svg.setAttribute('transform', `translate(${translateX},${translateY}) scale(${scale})`);
    }
});

svg.addEventListener('mouseup', (evt) => {
    isDragging = false;

    const minDistanceForDragging = 100;
    const distance = (startX - evt.clientX)**2 + (startY - evt.clientY)**2;

    if (distance < minDistanceForDragging) {
        ekranSuruklendi = false;
    }

});

svg.addEventListener('mouseleave', () => {
  //  isDragging = false;
   // console.log("mouse leaved");
});

// Fare tekerleği ile zoom in ve zoom out işlemi
svg.addEventListener('wheel', (e) => {
    e.preventDefault();
    const { offsetX, offsetY, deltaY } = e;
    const zoomFactor = deltaY > 0 ? (1 - scaleFactor) : (1 + scaleFactor);
    const scale_ = scale *  zoomFactor;
   // alert(zoomFactor);



    if (scale_ > scaleMin && scale_ < scaleMax ) {
        scale = scale_;
        const transform = svg.getAttribute('transform');
        let translateX = 0;
        let translateY = 0;
        if (transform) {
            const translate = transform.match(/translate\(([^)]+)\)/);
            if (translate) {
                [translateX, translateY] = translate[1].split(',').map(Number);
            }
        }

        // SVG'nin yeni merkezi hesaplanıyor
        const centerX = (offsetX - translateX) / scale;
        const centerY = (offsetY - translateY) / scale;


        const newTranslateX = offsetX - centerX * scale;
        const newTranslateY = offsetY - centerY * scale;

        svg.setAttribute('transform', `translate(${newTranslateX},${newTranslateY}) scale(${scale})`);
    //    svg.setAttribute('transform', `transform="translate(572,-497) scale(${scale})`);
    }

});


document.addEventListener('mouseup', function(event) {
    // Sadece sol tuş (0 değeri) için kontrol et
    if (event.button === 0) {
        isDragging = false;
    }
});



document.getElementById('footer').addEventListener("mouseenter", () => {
    isDragging = false;
})





let lastScale = 1;
let currentScale = 1;


svg.addEventListener('touchstart', (event) => {

    if (event.touches.length !== 1 )
        isDragging = false;

    if (event.touches.length === 2) {
        lastScale = getDistance(event.touches[0], event.touches[1]);
    }
});

svg.addEventListener("touchend", (event) => {
   //alert("t end" + event.touches.length);
});

svg.addEventListener('touchmove', (event) => {
    if (event.touches.length === 2) {
        event.preventDefault(); // Sayfanın kaymasını engelle
       // alert(event.touches.length);
        currentScale = getDistance(event.touches[0], event.touches[1]);


         //alert("x: " + event.touches.length);
        zoomFactor = currentScale / lastScale ;

        const scale_ = scale * zoomFactor;


                if (scale_ > scaleMin && scale_ < scaleMax ) {
                    scale = scale_;
                    const transform = svg.getAttribute('transform');
                    let translateX = 0;
                    let translateY = 0;
                    if (transform) {
                        const translate = transform.match(/translate\(([^)]+)\)/);
                        if (translate) {
                            [translateX, translateY] = translate[1].split(',').map(Number);
                        }
                    }


                    let offsetX = 1000;
                    let offsetY = 1000;

                    // SVG'nin yeni merkezi hesaplanıyor
                    const centerX = (offsetX - translateX) / scale;
                    const centerY = (offsetY - translateY) / scale;

                    const newTranslateX = offsetX - centerX * scale;
                    const newTranslateY = offsetY - centerY * scale;

                    svg.setAttribute('transform', `translate(${newTranslateX},${newTranslateY}) scale(${scale})`);
                  //  svg.setAttribute('transform', `transform="translate(572,-497) scale(${scale})`);
                    //alert("zoom in :" + scale);
                    lastScale = currentScale;

                }

    }
});


function getDistance(touch1, touch2) {
    const dx = touch1.clientX - touch2.clientX;
    const dy = touch1.clientY - touch2.clientY;
    return Math.sqrt(dx * dx + dy * dy);
}

