
class ArmyManager {
    constructor() {}
    minPathSize = Infinity;

    moveArmy(from, to, army) {
        if (cityMap[from].isNeighbor(to)) {
            send("moveArmy", from + "-" + to + "-" + army );
            document.getElementById("create-army-div").style.visibility = "hidden";
        }
    }


    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    async move2(cityFrom, cityTo, armyCount) {
        //düşma şehri && ttfiak şehri && kendi şehri
        if (!(countryMap[player].isEnemy(cityMap[cityTo].countryId) || countryMap[player].isAlliance(cityMap[cityTo].countryId) || cityMap[cityTo].countryId === player)) {
            console.log("hatatt");
            clickedCityId = null;
            return;
        }


        clickedCityId = null;
        let path = this.findShortestPath(cityFrom, cityTo);
        let i = 0;
        let error = false;

        while (path !== null && i <= path.length -2) {

            while (i < path.length - 1 &&  cityMap[path[i]].getArmyForCountry(player) >= armyCount) {

                for (let j = i; j <= path.length - 2; j++) {
                    // ilk ve son node
                    if (j === i && j === path.length - 2) {
                        this.moveArmy(path[j], path[j + 1], 1);
                        this.drawMovingArmy(path[j].toString(), path[j + 1].toString(), true, armyCount);
                    }

                    //  ilk node
                    else if (j === i) {
                        this.moveArmy(path[j], path[j + 1], 1);
                        this.drawMovingArmy(path[j].toString(), path[j + 1].toString(), false, armyCount);
                    }

                    // son node
                    else if (j === path.length - 2) {
                        this.drawMovingArmy(path[j].toString(), path[j + 1].toString(), true);
                    } else {
                        this.drawMovingArmy(path[j].toString(), path[j + 1].toString(), false);
                    }
                }

                await this.sleep(5000);
                error = false;
                i++;

            }


           if (error) break;


           for (let x = 0; x < 20; x++) {
               await this.sleep(50);
               if (i < path.length && cityMap[path[i]].getArmyForOwnCountry() >= armyCount) {
                   break;
               }
           }

           error = true;
       }

    }





    findShortestPath(cityFromId, cityToId, deep = 1, visitedCities = []) {
        if (deep === 1) this.minPathSize = Infinity;

        deep++;
        if (deep > this.minPathSize) return null;



        const cityFrom = cityMap[cityFromId];
        const cityTo = cityMap[cityToId];


        visitedCities.push(cityFrom.id);


        const neighbors = cityFrom.neighbors; // Komşu şehirler
        let newPath = [];
        let tmpList = [];
        let length = Infinity;

        for (const neighbor of neighbors) {
            if (neighbor === cityTo.id) {
                visitedCities.push(neighbor);
                return [...visitedCities]; // Klonlama için spread operatorü
            }
            else if (!visitedCities.includes(neighbor) && cityMap[neighbor].countryId !== undefined) {
                const city = cityMap[neighbor];


                if (city.countryId !== undefined && (countryMap[player].isEnemy(city.countryId) || city.countryId === cityFrom.countryId || countryMap[cityFrom.countryId].isAlliance(city.countryId))) {

                    tmpList = this.findShortestPath(city.id, cityTo.id, deep, [...visitedCities]); // Klonlanmış listeyi gönder

                    // Şehir bulunmuşsa
                    if (tmpList !== null) {
                        // Daha kısa bir yol
                        if (length > tmpList.length) {
                            newPath = tmpList;
                            length = newPath.length;
                        }
                    }
                }
            }
        }

        // Eğer hedef şehir bulunmuşsa, en kısa yolu döndür
        if (newPath.length > 0 && newPath[newPath.length - 1] === cityTo.id) {
            this.minPathSize = newPath.length;
            return [...newPath];
        }

        // Hiçbir yol bulunamadıysa null döndür
        return null;
    }






    drawMovingArmy(cityFrom, cityTo, drawHead, armyCount ) {
        let fromElem = null, toElem = null;

        let circles = document.getElementById("label_points").querySelectorAll("circle");
        let circleId = null;
        for (let circle of circles) {
            circleId = circle.getAttribute("id");
            if (circleId === cityFrom) {
                fromElem = circle;
            }
            else if (circleId === cityTo) {
                toElem =  circle;
            }
        }


        // Koordinatları al
        const startX = parseFloat(fromElem.getAttribute('cx')) ;
        const startY = parseFloat( fromElem.getAttribute('cy'));
        const endX = parseFloat( toElem.getAttribute('cx') );
        const endY = parseFloat(toElem.getAttribute('cy'));





        const svg = document.getElementById("map");
        const color = "#edb9b9";

        // Çizgi ekleme
        let line = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line.setAttribute("x1", startX);
        line.setAttribute("y1", startY);
        line.setAttribute("x2", endX);
        line.setAttribute("y2", endY);
        line.setAttribute("stroke", color);
        line.setAttribute("stroke-width", "1");
        line.setAttribute("stroke-dasharray", "10,5"); // Kesik çizgi efekti
        line.setAttribute("stroke-dashoffset", "0");
        line.style.animation = "dashAnimation 1s linear infinite"; // Animasyon ekleme
        svg.appendChild(line);


        let arrowHead = null, rect = null, text = null;

        if (drawHead) {
            // Ok başı ekleme
            arrowHead = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
            const arrowSize = 5;
            const angle = Math.atan2(endY - startY, endX - startX);

            const arrowX1 = endX - arrowSize * Math.cos(angle - Math.PI / 6);
            const arrowY1 = endY - arrowSize * Math.sin(angle - Math.PI / 6);

            const arrowX2 = endX - arrowSize * Math.cos(angle + Math.PI / 6);
            const arrowY2 = endY - arrowSize * Math.sin(angle + Math.PI / 6);

            arrowHead.setAttribute("points", `${endX},${endY} ${arrowX1},${arrowY1} ${arrowX2},${arrowY2}`);
            arrowHead.setAttribute("fill", color);
            svg.appendChild(arrowHead);
        }

        if (armyCount !== undefined) {
            // Orta noktaya sayı ekleme
            const midX = (startX + endX) / 2;
            const midY = (startY + endY) / 2;

            text = document.createElementNS("http://www.w3.org/2000/svg", "text");
            text.setAttribute("x", midX);
            text.setAttribute("y", midY);
            text.setAttribute("fill", "yellow");
            text.setAttribute("font-size", "6");
            text.setAttribute("text-anchor", "middle");
            text.setAttribute("dominant-baseline", "middle");
            text.textContent = flagMap[player.toString()] +  armyCount.toString();
            svg.appendChild(text);


            rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
            rect.setAttribute('x', midX - 6 );
            rect.setAttribute('y', midY - 3);
            rect.setAttribute('width', (text.getBBox().width +1).toString());
            rect.setAttribute('height', (text.getBBox().height -2).toString());
            svg.appendChild(rect);
            svg.appendChild(text);

        }

        setTimeout(() => {
            line.remove();
            if (rect !== null) rect.remove();
            if (text !== null) text.remove();
            if (arrowHead !== null) arrowHead.remove();

        }, 5050);


    }





}


function createArmyButtonClick() {
    if (clickedCityId !== null) {
        cityManager.hideMoveArmyButton();
        document.getElementById("fortress-button").remove();
        const ball = document.getElementById('ball');
        const slider = document.getElementById('slider');
        const sliderWidth = slider.offsetWidth - ball.offsetWidth; // Boncuğun kayabileceği maksimum genişlik
        let amount = Math.round((parseInt(ball.style.left) / sliderWidth) * 10);
        send("createArmy", clickedCityId + "-" + amount);
        document.getElementById("create-army-div").style.visibility = "hidden";
        removeArrows();
        clickedCityId = null;
    }
}
