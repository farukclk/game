/*
*
*
*  */


//const socket = new MyWebSocket("ws://192.168.122.1:5454");
const socket = new MyWebSocket("wss://farukclk.chickenkiller.com:5454");

const availableCountries = new Set()

const cityMap = {};
const countryMap = {};

let countryColorMap = {"1": "#9c7e00", "2": "#cf2222", "3": "#009f25", "4": "#94ff16", "5": "#4e5459", "6": "#f4d32b", "7": "#471570"}

let flagMap = {1: "🇬🇷", 2: "\uD83C\uDDEF\uD83C\uDDF5", 3 : "\uD83C\uDDF9\uD83C\uDDF7", 4: "\uD83C\uDDF9\uD83C\uDDF7", 5: "\uD83C\uDDF9\uD83C\uDDF7", 6: "\uD83C\uDDF9\uD83C\uDDF7", 7: "\uD83C\uDDF9\uD83C\uDDF7"};

let data = ""

let mouseOverCityId = null;
let player = 0; // country ıd


let originalColor;
let lightColor;
let money = 0;
const cityManager  = new CityManager()
const countryManager = new CountryManager();



function send(command, data) {
    socket.send(JSON.stringify({command: command, data: data}));
}


// sunucudan seçilebilir ulke listesini çek
function selectCountry() {
    send("getIdleCountryList");
}



function setNewCountry(countryId) {
    player = countryId;
    send("setNewCountry", countryId);
}


function showModal() {

    const modal = document.getElementById('myModal');
    const countryList = document.getElementById('countryList');

    // Ülke listesini temizle
    countryList.innerHTML = '';

    // Ülke listesini oluştur
    availableCountries.forEach(country => {

        const button = document.createElement('button');
        button.textContent = country.name;
        button.classList.add('country-button');
        button.setAttribute('data-id', country.id);
        button.onclick = function() {

            setNewCountry(country.id);
            modal.style.display = 'none';
        };
        countryList.appendChild(button);
    });

    // Modalı göster
    modal.style.display = 'block';
}

// Modalı kapatmak için
document.getElementsByClassName('close')[0].onclick = function() {
    const modal = document.getElementById('myModal');
    modal.style.display = 'none';
};

// Modalı, kullanıcı modal dışına tıklarsa kapat
window.onclick = function(event) {
    const modal = document.getElementById('myModal');
    if (event.target === modal) {
        modal.style.display = 'none';
    }
};



function loadMap() {
    const svg = document.querySelector('svg');
    const cities = svg.querySelectorAll('circle');

    // sunucudan gelen
    cities.forEach((city) => {
        cityMap[city.id] = new City(parseInt(city.id), document.getElementById(city.id).getAttribute("name"));
    });



}

function main() {

    const element = document.querySelector('#features');
    const info = document.querySelector('.il-isimleri');

    element.addEventListener(
        'mouseover',
        function (event) {

            if (event.target.tagName === 'path') {
                info.innerHTML = [
                    '<div>',
                    event.target.getAttribute("name"),
                    event.target.getAttribute("id"),
                    '</div>'
                ].join('');
            }


            if (cityMap.hasOwnProperty(event.target.id))
                event.target.style.fill = lightenColor(countryColorMap[cityMap[event.target.id].countryId]);
            else
                event.target.style.fill = "#000054";

                mouseOverCityId = event.target.id;

        });



    element.addEventListener(
        'mousemove',
        function (event) {
          info.style.top = event.pageY + 25 + 'px';
          info.style.left = event.pageX + 'px';
        }
      );



    element.addEventListener(
        'mouseout', function (event) {
        info.innerHTML = '';
        let path = event.target;

        if (event.target.tagName === 'path' && cityMap.hasOwnProperty(path.id) ){
            path.style.fill = countryColorMap[cityMap[path.id].countryId];
        }
        else {
            path.style.fill = "#002a1d";
        }
    });

    element.addEventListener(
        'click',
        function (event) {

            const id = event.target.id;

            if (ekranSuruklendi) {
                ekranSuruklendi = false;
                return;
            }

            //------
            removeArrows();


            ekranSuruklendi = false;

            let city = cityMap[id];

            if (city !== undefined) cityManager.clickedCity(event.target);
            else cityManager.clickedCityId = null;

        });
}





function createArmy(areaName, n, m, pathId) {

    n--;

    let path = document.getElementById("armies");
    let c = document.getElementById("label_points").getElementsByClassName(areaName)[0];

    try {

        const text = createText(c.getAttribute("cx"), parseInt(c.getAttribute("cy")) + 6 * n + 2, m, pathId)
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        rect.setAttribute('x', c.getAttribute("cx"));
        rect.setAttribute('y', (parseInt(c.getAttribute("cy")) + 6 * n -1).toString());
        path.appendChild(text);
        rect.setAttribute('width', text.getBBox().width.toString());
        rect.setAttribute('height', (text.getBBox().height -1).toString());
        path.appendChild(rect);
        path.appendChild(text);
    }
    catch (err) {
        console.log(areaName);
    }


}




function createText(x, y, m, pathId) {
    const text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    text.setAttribute("id", "text" + pathId)
    text.setAttribute('x', x);
    text.setAttribute('y', y);
    text.setAttribute('font-size', '5px');  // Yazı boyutunu burada ayarlayın
    text.setAttribute('fill', 'yellow');       // Yazı rengini burada ayarlayın
    text.setAttribute('dominant-baseline', 'middle');
    text.setAttribute('fill', 'yellow');       // Yazı rengini burada ayarlayın
    text.setAttribute('font-weight', 'bold'); // Yazı kalınlığını burada ayarlayın
    text.textContent =  m;

    return text;

}




function updatePopulationMap() {

    cityManager.removeInterval(clickedCityId);
    // tüm askerleri sil tekrar olustur
    let a = document.getElementById("armies");
    a.remove();
    const new_a = document.createElementNS("http://www.w3.org/2000/svg", "g");
    new_a.setAttribute("id", "armies");
    document.getElementById("map").appendChild(new_a);


    const paths = document.getElementById("features").querySelectorAll('path');

    paths.forEach((path, index) => {

        if (cityMap.hasOwnProperty(path.id)) {

            let city = cityMap[path.id];

            path.style.fill = countryColorMap[city.countryId];


            let i = 1;
            Object.keys(city.armyMap).forEach(countryId => {
                if (city.armyMap[countryId] !== 0) {
                    createArmy(path.getAttribute("name"), i++, flagMap[countryId] + city.armyMap[countryId], path.id )
                }
            });
        }
    });

}




function removeArrows() {
    // remove previous arrows
    const previousArrows = document.querySelectorAll(".arrowPath");
    previousArrows.forEach(arrow => {
        arrow.remove();
    })
}




function drawArrow(from, to ) {
    let fromElem = null, toElem = null;

    let circles = document.getElementById("label_points").querySelectorAll("circle");
    let circleId = null;
    for (let circle of circles) {
        circleId = circle.getAttribute("id");
        if (circleId === from) {
            fromElem = circle;
        }
        else if (circleId === to) {
            toElem =  circle;
        }
    }


    // Koordinatları al
    const fromX = fromElem.getAttribute('cx') ;
    const fromY = fromElem.getAttribute('cy');
    const toX = toElem.getAttribute('cx') ;
    const toY = toElem.getAttribute('cy');


    // Yeni bir path elementi oluştur
    const path = document.createElementNS("http://www.w3.org/2000/svg", "path");

    // Path'i çiz
    path.setAttribute("class", "arrowPath");
    path.setAttribute('d', `M${fromX},${fromY} L${toX},${toY}`);
    path.setAttribute('stroke', 'white');
    path.setAttribute('stroke-width', '2');
    path.setAttribute('marker-end', 'url(#arrow)');

    // SVG'ye path'i ekle
    document.getElementById('map').appendChild(path)

}




function lightenColor(color, percent) {
    color = color ? color : "#002a1d";
    try {
        percent = 40;
        const R = Math.max(0, parseInt(color.slice(1, 3), 16) - percent );
        const G = Math.max(0, parseInt(color.slice(3, 5), 16) - percent );
        const B = Math.max(0, parseInt(color.slice(5, 7), 16) - percent );

        return "#" + (0x1000000 + (R << 16) + (G << 8) + B).toString(16).slice(1);
    }
    catch (err) {
        console.log(color);
    }

}
