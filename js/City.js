class City {
    name;
    countryId;
    economy;
    population;
    neighbors;
    fortress;
    armyMap = {};


    constructor(id, name) {
        this.id = id;
        this.name = name;
        for (let i = 0 ; i < 10; i++ ) {this.armyMap[i] = 0}
    }

    updateCity(countryId, neighbors, economy, population, fortress, armyMap) {
        this.countryId = countryId;
        this.neighbors = neighbors;
        this.economy = economy;
        this.population = population;
        this.fortress = fortress;
        if (armyMap !== undefined)
            this.armyMap = armyMap;
    }


    getArmyForOwnCountry() {
        return this.armyMap[this.countryId];
    }

    getArmyForCountry(countryId) {
        return this.armyMap[countryId];
    }

    isNeighbor(cityId) { return this.neighbors.includes(parseInt(cityId)); }

}

class Army {
    countryId;
    army;

    constructor(countryId, army) {
        this.countryId = countryId;
        this.army = army;
    }
}
