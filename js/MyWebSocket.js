

class MyWebSocket {
    constructor(url) {
        this.url = url;
        this.socket = null;
        this.listeners = {};
        this.isConnected = false;
        this.connect();
    }

    connect() {
      // alert("connecting...");
        this.socket = new WebSocket(this.url);

        this.socket.onopen = () => {
            this.isConnected = true;
            this.triggerEvent('open');
            localStorage.setItem("socketState", "open");
            console.log('WebSocket bağlantısı başarıyla açıldı.');

            //send("getIdleCountryList");
            //setNewCountry(2)

        };

        this.socket.onmessage = (event) => {

            this.triggerEvent('message', event.data);
        //     console.log('Sunucudan gelen mesaj:', event.data);

            let command = JSON.parse(event.data)[0];
            let a;

            if (command === "createCountries") {
                a = JSON.parse(JSON.parse(event.data)[1]);

                availableCountries.clear();
                for (let i = 1; i <= 7 ; i++) {
                    countryMap[a[i].id] = new Country(a[i].id, a[i].name, a[i].playerId, a[i].money, a[i].enemies, a[i].cityList);
                }
            }

            else if (command === "selectCountry") {
                console.log("selectCountry");
                a = JSON.parse(JSON.parse(event.data)[1]);

                availableCountries.clear();
                for (let i = 1; i <= 7 ; i++) {
                    if (a[i]["playerId"] === 0  && a[i]["cityList"].length > 0 ) {
                        availableCountries.add(a[i]);
                    }

                    countryMap[a[i].id] = new Country(a[i].id, a[i].name, a[i].playerId, a[i].money, a[i].enemies, a[i].cityList);
                }
                showModal();
            }

            else if (command === "peaceRequest") {
                let countryId = JSON.parse(event.data)[1];
                console.log("peace request from " + countryMap[countryId].name);

                let randomId = Math.floor( Math.random() * 10**6)

                messageMap.set(randomId, new Message("peaceRequest", countryId));
                addMessageBox(randomId);

            }

            else if (command === "allianceRequest") {
                let countryId = JSON.parse(event.data)[1];
                console.log("alliance request from " + countryMap[countryId].name);

                let randomId = Math.floor( Math.random() * 10**6)

                messageMap.set(randomId, new Message("allianceRequest", countryId));
                addMessageBox(randomId);
            }

            else if (command === "allianceRequestAccepted") {
                let countryId = JSON.parse(event.data)[1]; // barış teklifini kabul eden ulke
                alert(countryMap[countryId].name + " ittifak klifinizi kabul etti");
            }

            else if (command === "allianceRequestRejected") {
                let countryId = JSON.parse(event.data)[1]; // barış teklifini reddeden eden ulke
                alert(countryMap[countryId].name + " ittfak teklifinizi reddetti");
            }

            else if (command === "removeAlliance") {
                let countryId = JSON.parse(event.data)[1];
                alert(countryMap[countryId].name + " ittfaktan ayrıldı !");
            }

            else if (command === "peaceAccepted") {
                let countryId = JSON.parse(event.data)[1]; // barış teklifini kabul eden ulke
                alert(countryMap[countryId].name + " barış teklifinizi kabul etti");
            }

            else if (command === "peaceRejected") {
                let countryId = JSON.parse(event.data)[1]; // barış teklifini reddeden eden ulke
                alert(countryMap[countryId].name + " barış teklifinizi reddetti");
            }

            else if (command === "warDeclared") {
                let countryId = JSON.parse(event.data)[1];
                alert(countryMap[countryId].name + " size karşı savaş ilan etti ! ");
            }

            else if (command === "updateCountries") {
                a = JSON.parse(JSON.parse(event.data)[1]);

                let i = 1;
                try {
                    for (; i <= Object.keys(countryColorMap).length   ; i++) {
                        countryMap[a[i].id].updateCountry(a[i].playerId, a[i].enemies, a[i].cityList, a[i].alliances, a[i].money);
                    }
                }
                catch (e) {
                    console.log("hata : " + i);
                }
            }

            else if (command === "countrySuccessfullySelected") {
                 money = parseInt( JSON.parse(event.data)[1] );
                 startGameScreen();

            }

            else if (command === "countryNotSelected") {
                player = 0;
                let country = JSON.parse(event.data)[1];
                alert(`${country} ulkesi seçilemedi, başka bir oyuncu kullanıyor olabilir[!]`);
            }


            // update online player count
            else if (command === "updateOnlinePlayers") {
                let element = document.getElementById("online-player-count");
                data =  JSON.parse(event.data)[1];
                element.textContent = "online " + data;
            }

            else if (command === "updateMoney") {
                let element = document.getElementById("money-box");
                data =  JSON.parse(event.data)[1];
                element.textContent = data + " 💰";
            }


            else if (command === "updateMyCities") {
                cityManager.updateCities(JSON.parse(event.data)[1]);
                updatePopulationMap();
            }


            else if (command === "updateCities") {

                cityManager.updateCities( JSON.parse(event.data)[1]);
                const paths = document.getElementById("features").querySelectorAll('path');

                paths.forEach((path) => {

                    try {
                        if (mouseOverCityId !== path.id)
                            path.style.fill = countryColorMap[cityMap[path.id].countryId];
                    }
                    catch (error) {}
                });

            }

        };

        this.socket.onclose = () => {
            this.triggerEvent('close');
            this.isConnected = false;

           // alert("Bağlantı kesildi [!]");
          //  location.reload();

            if (event.wasClean) {
                console.log(`WebSocket bağlantısı kapatıldı, kod: ${event.code}, neden: ${event.reason}`);
            } else {
                console.error('Bağlantı kesildi'); // bağlantı kopuk

            }

        };

        this.socket.onerror = (error) => {
            this.triggerEvent('error', error);
            this.isConnected = false;
            console.error('WebSocket hatası:', error.message);
        };
    }

    send(message) {
        if (this.socket && this.socket.readyState === WebSocket.OPEN) {
            this.socket.send(message);
        } else {
            console.error('WebSocket is not open. Ready state: ' + this.socket.readyState);
        }
    }

    close() {
        if (this.socket) {
            this.socket.close();
        }
    }

    on(event, listener) {
        if (!this.listeners[event]) {
            this.listeners[event] = [];
        }
        this.listeners[event].push(listener);
    }

    triggerEvent(event, data) {
        if (this.listeners[event]) {
            this.listeners[event].forEach(listener => listener(data));
        }
    }
}

