
let clickedCityId = null
let blinkInterval;
class CityManager {

    isLight = true;
    moveArmyButtonClicked = false;

    constructor() {
    }

    clickedCity(path) {

        let city = cityMap[path.id];
        let id = city.id;



        if (document.getElementById("declare-war-button") !== null) document.getElementById("declare-war-button").remove();
        if (document.getElementById("send-peace-request-button") !== null) document.getElementById("send-peace-request-button").remove();
        if (document.getElementById("send-alliance-request-button") !== null) document.getElementById("send-alliance-request-button").remove();
        if (document.getElementById("fortress-button") !== null) document.getElementById("fortress-button").remove();
        if (!cityManager.moveArmyButtonClicked) this.hideMoveArmyButton();

        document.getElementById("create-army-div").style.visibility = "hidden";


        this.removeInterval(clickedCityId);
        removeArrows();


        if (city.countryId === undefined || city.countryId === null) {
            clickedCityId = null;
        }


        else if (player === 0) {
            let countryId = city.countryId;

            let onay = confirm(countryMap[countryId].name + " ülkesini seçmek üzeresiniz");
            if (onay) {
                setNewCountry(countryId);
            }
        }


        else if (parseInt(clickedCityId) === id) { // aynı sehre 2. ekz tıklama

            console.log("aynı sheir");
            this.moveArmyButtonClicked = false
            this.hideMoveArmyButton();
            this.hideDeclareWarButton();
            clickedCityId = null;
        }

        else if (city.countryId === player) {  // kendi sehri

            if (clickedCityId === null) { // ilk tıklama
                this.drawArrows(id);
                this.createInterval(path);
                clickedCityId = id;
                this.showBuildFortressButton();
                this.showMoveArmyButton();
            }
            // uzun mesafeli
            else if (cityManager.moveArmyButtonClicked) {
                armyManager.move2(clickedCityId, city.id, 1).then(r => r);
                this.hideMoveArmyButton();
            }

            else if (city.isNeighbor(clickedCityId) &&  cityMap[clickedCityId].getArmyForCountry(player) > 0 && (countryMap[cityMap[clickedCityId].countryId].isAlliance(player) || cityMap[clickedCityId].countryId === player )) {
                armyManager.move2(clickedCityId, city.id, 1).then(r => r);
                clickedCityId = null;
            }

            else {
                clickedCityId = city.id;
                this.createInterval(path);
                this.drawArrows(id);
                this.showBuildFortressButton();
                this.showMoveArmyButton();
            }

        }

        else {  // başka oyuncunun şehri
        //    console.log(city);
            if (clickedCityId !== null && cityMap[clickedCityId].getArmyForCountry(player) > 0 && city.isNeighbor(clickedCityId) ) {
                if (countryMap[city.countryId].isEnemy(player) || countryMap[city.countryId].isAlliance(player) || city.countryId === player) {
                    armyManager.move2(clickedCityId, id, 1).then(r => r);
                    clickedCityId = null;
                }
                else {
                    this.createInterval(path);
                    this.showDeclareWarButton();
                    this.showAllianceButton();
                    clickedCityId = city.id;
                    this.showBuildFortressButton();
                    this.showMoveArmyButton();
                }
            }

            // uzun mesafeli
            else if (cityManager.moveArmyButtonClicked  ) {
                armyManager.move2(clickedCityId, city.id, 1).then(r => 1);
                this.hideMoveArmyButton();
            }

            else {
                clickedCityId = city.id;

                this.createInterval(path);
                if (countryMap[city.countryId].isEnemy(player)) {  // düşman
                    this.showPeaceButton();
                    this.showBuildFortressButton(clickedCityId);
                }

                else if (countryMap[city.countryId].isAlliance(player)) { // ittifak ülke
                    this.showAllianceButton();

                    if (city.getArmyForCountry(player) > 0) {
                        this.drawArrows(city.id);
                        this.showMoveArmyButton();
                    }
                }

                else {

                    this.showDeclareWarButton();
                    this.showAllianceButton();

                }

            }

        }
    }



    showBuildFortressButton() {
        let button = document.createElement("button");
        button.className = "side-bar-buttons"
        button.id = "fortress-button";

        let city = cityMap[clickedCityId];


        if (city === undefined || city === null || city.countryId === null)
            return;

        // kendi şehrimiz
        if (city.countryId === player) {
            if (city.fortress === 0) {
                button.innerText = "sur inşa et";
                button.onclick = function () {
                    if (countryMap[player].money >= 5) {
                        let onay = confirm("5 para karşılığında sur inşa et")
                        if (onay) {
                            send("buildFortress", clickedCityId);
                            document.getElementById("fortress-button").remove();
                            cityManager.showBuildFortressButton();
                        }
                    }
                }
                document.getElementById("side-area").appendChild(button);
            }

            else if (city.fortress < 3) {
                button.innerText = "suru güçlendir";
                button.onclick = function () {
                    if (countryMap[player].money >= 5) {
                        let onay = confirm("5 para karşılığında suru güçlendir")
                        if (onay) {
                            send("upgradeFortress", clickedCityId);
                            document.getElementById("fortress-button").remove();
                            cityManager.showBuildFortressButton();
                        }
                    }
                }
                document.getElementById("side-area").appendChild(button);
            }
        }
        else {
            if (city.fortress !== 0) {
                button.innerText = "suru yık";
                button.onclick = function () {
                    send("destroyFortress", clickedCityId);
                    document.getElementById("fortress-button").remove();
                }
                document.getElementById("side-area").appendChild(button);
            }
        }
    }


    showAllianceButton() {
        let button = document.createElement("button");
        button.className = "side-bar-buttons"
        if (countryMap[player].isAlliance(cityMap[clickedCityId].countryId)) { // ittifak ülkenin toprağı
            button.id = "send-alliance-request-button";
            button.innerText = "ittifaktan ayrıl";
            button.onclick = function () {
                cityManager.removeInterval(clickedCityId);
                countryManager.leaveAlliance(cityMap[clickedCityId].countryId);
                document.getElementById("send-alliance-request-button").remove();
                clickedCityId = null;
            }
            document.getElementById("side-area").appendChild(button);
        }

        else if ( ! countryMap[player].hasAlliance()) {
            button.id = "send-alliance-request-button";
            button.innerText = "ittif teklif et";
            button.onclick = function () {
                cityManager.removeInterval(clickedCityId);
                countryManager.sendAllianceRequestTo(cityMap[clickedCityId].countryId);
                document.getElementById("send-alliance-request-button").remove();
                clickedCityId = null;
            }
            document.getElementById("side-area").appendChild(button);
        }

    }




    showPeaceButton() {
        let button = document.createElement("button");
        button.id = "send-peace-request-button";
        button.innerText = "barış isteği gönder";
        button.className = "side-bar-buttons"


        button.onclick = function () {
            cityManager.removeInterval(clickedCityId);
            countryManager.sendPeaceRequestTo(cityMap[clickedCityId].countryId);
            document.getElementById("send-peace-request-button").remove();
            clickedCityId = null;
        }

        document.getElementById("side-area").appendChild(button);
    }


    showDeclareWarButton() {
        // Yeni bir <div> öğesi oluşturun
        let button = document.createElement("button");
        button.id = "declare-war-button";
        button.innerText = "savaş ilan et";
        button.className = "side-bar-buttons"


        button.onclick = function () {
            cityManager.removeInterval(clickedCityId);
            countryManager.declareWarBetween(cityMap[clickedCityId].countryId);
            document.getElementById("declare-war-button").remove();
            clickedCityId = null;
        }

        document.getElementById("side-area").appendChild(button);
    }



    hideDeclareWarButton() {
        let a = document.getElementById("declare-war-button");
        if (a !== null) { a.remove(); }
     }



     hideMoveArmyButton() {

         let a = document.getElementById("move-army-button");
         if (a !== null)
             a.remove();
         cityManager.moveArmyButtonClicked = false;
     }




     showMoveArmyButton() {
        if (cityManager.moveArmyButtonClicked) {
            document.getElementById("move-army-button").innerText = "harketi et";
            this.moveArmyButtonClicked = false;

        }
        else {
            let button = document.createElement("button");
            button.id = "move-army-button";
            button.innerText = "hareket et";
            button.className = "side-bar-buttons"


            button.onclick = function () {
                if (cityManager.moveArmyButtonClicked) {
                    document.getElementById("move-army-button").innerText = "harket et";
                }
                else {
                    document.getElementById("move-army-button").innerText = "harketi iptal et";
                }

                cityManager.moveArmyButtonClicked = ! cityManager.moveArmyButtonClicked;

            }

            document.getElementById("side-area").appendChild(button);
        }
     }


    drawArrows(id) {
        let city = cityMap[id];

        if (city.countryId === player || countryMap[city.countryId].isAlliance(player)) { // kendi sehrim veya müttefiğimin şehri
            document.getElementById("create-army-div").style.visibility = "visible";

            if (city.getArmyForCountry(player) > 0) { // ordu var

                city.neighbors.forEach(neighbor => {
                    try {
                       if (cityMap[neighbor].countryId === player || countryMap[cityMap[neighbor].countryId].isEnemy(player) || countryMap[cityMap[neighbor].countryId].isAlliance(player))
                            drawArrow(id.toString() , neighbor.toString());
                    }
                    catch (e) {
                      //  console.log("hata: " + e.toString());

                    }
                })
            }
        }
    }


    createInterval(path) {
        clickedCityId = path.id;

        originalColor = countryColorMap[cityMap[path.id].countryId]; // Orijinal rengi al
        lightColor = lightenColor(originalColor, 90); // %20 daha açık renk
        path.style.fill = lightColor;
        path.style.strokeWidth = 2;
        path.style.stroke = "#ffc400";
        blinkInterval = setInterval(() => {
            originalColor = countryColorMap[cityMap[path.id].countryId]; // Orijinal rengi al
            lightColor = lightenColor(originalColor, 90); // %20 daha açık renk
            path.style.fill = this.isLight ? originalColor : lightColor;
            this.isLight = !this.isLight;
        }, 400); // 500 ms aralıklarla değişti
    }


    removeInterval(clickedCityId) {
        if (clickedCityId !== null && clickedCityId !== undefined) {
            clearInterval(blinkInterval);
            document.getElementById(clickedCityId).style.fill = countryColorMap[cityMap[clickedCityId].countryId]; // eski rengine dondur
            document.getElementById(clickedCityId).style.strokeWidth = 1;
            document.getElementById(clickedCityId).style.stroke = "#382222";
        }

    }


    updateCities(data) {
        let cities = JSON.parse(data);

        Object.values(cities).forEach(city => {
            cityMap[city.id].updateCity(city.countryId, city.neighbors, city.economy, city.population, city.fortress, city.armyMap);
        });

    }


}
