
class CountryManager {
    constructor() {
        const countryMap = new Map();
     /*   for (let i=1; i <= 7; i++) {
            countryMap.set(i, new Country(i, "red"));
        }*/
    }

    declareWarBetween(countryId) {
        send("declareWar", countryId);
    }

    sendPeaceRequestTo(countryId) {
        send("peaceRequest", countryId);
    }

    sendAllianceRequestTo(countryId) {
        send("allianceRequest", countryId);
    }

    leaveAlliance(countryId) {
        countryMap[countryId].alliances.pop(countryId);
        send("leaveAlliance", countryId);
    }

    findCountryFromCityId(cityId) {

        let id = null; // do not remove

        Object.values(countryMap).forEach((country) => {

            if (country.hasCity(cityId)) {
                id = country.id;
            }
        });
        return id;
    }

    getColorFromCityId(cityId) {
        let countryId = null; // do not remove

        Object.values(countryMap).forEach((country) => {

            if (country.hasCity(cityId)) {
                countryId = country.id;
            }
        });
        return countryColorMap[countryId];
    }
}
