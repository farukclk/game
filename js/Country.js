

class Country {
    id
    name;
    color;
    playerId;
    money;
    enemies = [];
    cityList = [];
    alliances = [];


    constructor(id, name, playerId, money,  enemies, cityList) {
        this.id = id;
        this.name = name;
        this.playerId = playerId;
        this.money = money;
        this.enemies = enemies;
        this.cityList = cityList;
    }

    updateCountry(playerId, enemies, cityList, alliances, money) {
        this.playerId = playerId;
        this.enemies = enemies;
        this.cityList = cityList;
        this.alliances = alliances;

        if (this.money !== money) {
            this.money = money;
            document.getElementById("money-box").innerText = this.money + " 💰";
        }
    }


    isEnemy(countryId) {
        return this.enemies.includes(countryId);
    }


    isAlliance(countryId) {
        return this.alliances.includes(countryId);
    }

    hasAlliance() {
        return this.alliances.length > 0;
    }

    hasCity(cityId) {
        return this.cityList.includes(parseInt(cityId));
    }
}
